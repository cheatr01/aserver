package com.example;

import cz.svetonaut.aserver.framework.GodServlet;
import cz.svetonaut.aserver.framework.WebPath;
import org.glassfish.grizzly.Buffer;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

import java.io.IOException;
import java.io.Writer;

@WebPath(path = "/example")
public class ExampleServlet implements GodServlet {

	@Override
	public void doGet(Request request, Response response) {
		try {
			response.setContentType("plain/text");
			final Writer writer = response.getWriter();
			writer.write("Response from my servlet.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doPost(Request request, Response response) {
		try {
			final Buffer buffer = request.getPostBody(request.getContentLength());
			final String content = buffer.toStringContent();
			response.setContentType("plain/text");
			final Writer writer = response.getWriter();
			writer.write("Hello " + content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
