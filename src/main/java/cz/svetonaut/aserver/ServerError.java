package cz.svetonaut.aserver;

public class ServerError extends Error {
	public ServerError() {
	}

	public ServerError(String message) {
		super(message);
	}

	public ServerError(String message, Throwable cause) {
		super(message, cause);
	}

	public ServerError(Throwable cause) {
		super(cause);
	}
}
