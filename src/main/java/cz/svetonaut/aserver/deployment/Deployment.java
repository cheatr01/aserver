package cz.svetonaut.aserver.deployment;

import cz.svetonaut.aserver.configuration.Configuration;
import cz.svetonaut.aserver.container.ServletMappingHandler;
import cz.svetonaut.aserver.deployment.extractor.Extractor;
import cz.svetonaut.aserver.framework.GodServlet;
import cz.svetonaut.aserver.framework.WebPath;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.JarFile;

public class Deployment {

	private final Path toDeploy;
	private final Configuration configuration;
	private final ClassLoader classLoader;
	Set<ServletMappingHandler<? extends GodServlet>> handlers = new HashSet<>();

	public Deployment(Path toDeploy, Configuration configuration) {
		this.toDeploy = toDeploy;
		this.configuration = configuration;
		try {
			this.classLoader = new URLClassLoader(new URL[]{toDeploy.toUri().toURL()}, this.getClass().getClassLoader());
		} catch (MalformedURLException e) {
			throw new DeploymentLoadingException(e);
		}
	}

	public void deploy() {
		try (final JarFile jarFile = new JarFile(toDeploy.toFile())) {

			final Extractor extractor = new Extractor(jarFile, configuration);
			final Path appPath = extractor.extract();
			System.out.println(appPath.getFileName());

			if (!Files.exists(appPath) || !Files.isDirectory(appPath)) {
				throw new IllegalStateException("Deployment root is not in right state.");
			}

			searchHandlers(appPath, appPath);

		} catch (DeploymentLoadingException e) {
			throw e;
		} catch (Exception e) {
			throw new DeploymentLoadingException(e);
		}

	}

	private void searchHandlers(Path dir, Path root) {
		try {
			Files.walk(dir)
					.filter(Files::isRegularFile)
					.filter(it -> it.toString().endsWith(".class"))
					.map(it -> loadClass(it, root))
					.forEach(clazz -> {
						Arrays.stream(clazz.getInterfaces())
								.filter(it -> it.getName().equals(GodServlet.class.getName()))
								.findAny()
								.ifPresent(it -> handlers.add(
												new ServletMappingHandler<>(
														getMappingPath(clazz),
														(Class<GodServlet>) clazz
												)
										)
								);
					});
		} catch (IOException e) {
			throw new DeploymentLoadingException(e);
		}
	}

	private Class<?> loadClass(Path file, Path root) {
		final String fileName = file.toString().replace(root.toFile().getPath() + "/", "");

		final String className = fileName.substring(0, fileName.indexOf(".class")).replace("/", ".");
		System.out.println("ClassName: " + className);

		final Class<?> aClass;
		try {
			aClass = Class.forName(className, true, classLoader);
			System.out.println("class loaded");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}

		return aClass;
	}

	private String getMappingPath(Class<?> aClass) {
		return Arrays.stream(aClass.getDeclaredAnnotations())
				.filter(it -> it.annotationType().getName().equals(WebPath.class.getName()))
				.findFirst()
				.map(it -> (WebPath) it)
				.map(WebPath::path)
				.orElseThrow();
	}

	public Set<ServletMappingHandler<? extends GodServlet>> getHandlers() {
		return handlers;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Deployment that = (Deployment) o;

		if (toDeploy.getFileName().equals(that.toDeploy.getFileName())) return true;

		return toDeploy.equals(that.toDeploy);
	}

	@Override
	public int hashCode() {
		return toDeploy.hashCode();
	}
}
