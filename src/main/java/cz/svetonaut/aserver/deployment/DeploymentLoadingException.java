package cz.svetonaut.aserver.deployment;

public class DeploymentLoadingException extends RuntimeException {
	public DeploymentLoadingException() {
	}

	public DeploymentLoadingException(String message) {
		super(message);
	}

	public DeploymentLoadingException(String message, Throwable cause) {
		super(message, cause);
	}

	public DeploymentLoadingException(Throwable cause) {
		super(cause);
	}
}
