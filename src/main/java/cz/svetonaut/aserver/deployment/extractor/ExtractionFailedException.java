package cz.svetonaut.aserver.deployment.extractor;

import cz.svetonaut.aserver.deployment.DeploymentLoadingException;

public class ExtractionFailedException extends DeploymentLoadingException {

	public ExtractionFailedException() {
	}

	public ExtractionFailedException(String message) {
		super(message);
	}

	public ExtractionFailedException(Throwable cause) {
		super(cause);
	}

	public ExtractionFailedException(String message, Throwable cause) {
		super(message, cause);
	}
}
