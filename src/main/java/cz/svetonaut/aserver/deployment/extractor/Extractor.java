package cz.svetonaut.aserver.deployment.extractor;

import cz.svetonaut.aserver.configuration.Configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Extractor {

	private final JarFile jarFile;
	private final Configuration configuration;

	public Extractor(JarFile jarFile, Configuration configuration) {
		if (jarFile == null || configuration == null) {
			throw new ExtractionFailedException("Extractor couldn't be initialized because some required field was null.");
		}
		this.jarFile = jarFile;
		this.configuration = configuration;
	}

	public Path extract() {
		final String targetPath = configuration.getDeplTargetPath();
		final String name = jarFile.getName();
		System.out.println(name);

		final String appDir = name.replaceFirst("app/", "").replace(".jar", "");
		final String appRootPath = targetPath + appDir;
		final Path rootDir = Path.of(appRootPath);

		if (Files.exists(rootDir)) {
			deleteDir(rootDir);
		}
		createDir(rootDir);

		final Enumeration<JarEntry> entries = jarFile.entries();
		while (entries.hasMoreElements()) {
			final JarEntry element = entries.nextElement();
			final Path file = Path.of(appRootPath + File.separator + element.getRealName());
			if (element.isDirectory()) {
				createDir(file);
			} else {
				try (InputStream is = jarFile.getInputStream(element);
				     FileOutputStream fos = new FileOutputStream(file.toFile())) {
					while (is.available() > 0) {
						fos.write(is.read());
					}
				} catch (Exception e) {
					throw new ExtractionFailedException(e);
				}
			}
		}
		return rootDir;
	}

	private void createDir(Path rootDir) {
		try {
			Files.createDirectory(rootDir);
		} catch (IOException e) {
			throw new ExtractionFailedException(e);
		}
	}

	public void deleteDir(Path dir) {
		if (Files.isDirectory(dir, LinkOption.NOFOLLOW_LINKS)) {
			try(DirectoryStream<Path> dirs = Files.newDirectoryStream(dir)) {
				for (Path path : dirs) {
					deleteDir(path);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			Files.delete(dir);
		} catch (IOException e) {
			throw new ExtractionFailedException(e);
		}
	}

}
