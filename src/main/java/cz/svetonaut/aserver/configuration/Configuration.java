package cz.svetonaut.aserver.configuration;

public final class Configuration implements Cloneable {

	private String deplSourcePath;
	private String deplTargetPath;

	Configuration() {
	}

	public String getDeplSourcePath() {
		return deplSourcePath;
	}

	void setDeplSourcePath(String deplSourcePath) {
		this.deplSourcePath = deplSourcePath;
	}

	public String getDeplTargetPath() {
		return deplTargetPath;
	}

	void setDeplTargetPath(String deplTargetPath) {
		this.deplTargetPath = deplTargetPath;
	}

	@Override
	public Configuration clone() {
		try {
			Configuration clone = (Configuration) super.clone();

			clone.setDeplSourcePath(deplSourcePath);
			clone.setDeplTargetPath(deplTargetPath);

			return clone;
		} catch (CloneNotSupportedException e) {
			throw new AssertionError();
		}
	}
}
