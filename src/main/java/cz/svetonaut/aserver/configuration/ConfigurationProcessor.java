package cz.svetonaut.aserver.configuration;

import cz.svetonaut.aserver.AServer;
import cz.svetonaut.aserver.ServerError;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ConfigurationProcessor {

	private final Configuration configuration = new Configuration();

	public Configuration prepare() {
		try (final InputStream stream = AServer.class.getClassLoader().getResourceAsStream("aserver.conf")) {
			if (stream == null) {
				System.out.println("Config file don't found!");
				throw new ServerError("Configuration file missing.");
			}

			final BufferedReader br = new BufferedReader(new InputStreamReader(stream));
			String line;

			while ((line = br.readLine()) != null) {
				if (line.startsWith("DEPLOYMENT_SOURCE_PATH")) {
					configuration.setDeplSourcePath(beautification(line));
				} else if (line.startsWith("DEPLOYMENT_TARGET_PATH")) {
					configuration.setDeplTargetPath(beautification(line));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return configuration.clone();
	}

	private String beautification(String line) {
		return line.substring(line.indexOf("=") + 1);
	}

}
