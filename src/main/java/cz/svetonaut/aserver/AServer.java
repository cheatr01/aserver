package cz.svetonaut.aserver;

import cz.svetonaut.aserver.configuration.Configuration;
import cz.svetonaut.aserver.configuration.ConfigurationProcessor;
import cz.svetonaut.aserver.container.ServletContainer;

import java.io.IOException;

public class AServer {

	public static void main(String[] args) throws IOException {
		final ConfigurationProcessor confProcessor = new ConfigurationProcessor();
		final Configuration configuration = confProcessor.prepare();

		final ServletContainer servletContainer = new ServletContainer(configuration);
		servletContainer.run(args);
	}

}
