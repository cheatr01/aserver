package cz.svetonaut.aserver.container;

import cz.svetonaut.aserver.ServerError;
import cz.svetonaut.aserver.configuration.Configuration;
import cz.svetonaut.aserver.container.deploy.DeployManager;
import cz.svetonaut.aserver.deployment.Deployment;
import cz.svetonaut.aserver.framework.GodServlet;
import org.glassfish.grizzly.http.server.HttpServer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class ServletContainer {

	public static final String HOST = "localhost";
	public static final int PORT = 8080;

	private final DeployManager deployManager;
	private final Configuration configuration;
	private final Set<Deployment> applications = new HashSet<>();

	public ServletContainer(Configuration configuration) {
		this.configuration = configuration;
		this.deployManager = new DeployManager(configuration);
	}

	public void run(String[] args) {
		HttpServer server = null;
		try {
			System.out.println("app_path: " + configuration.getDeplSourcePath());

			final Set<Deployment> deployments = deployManager.deployAll();
			applications.addAll(deployments);

			server = HttpServer.createSimpleServer(null, HOST, PORT);
			registerServletsMappingHandlers(server);

			server.start();
			System.out.println("Press any key to stop the server...");
			final String nextLine = new Scanner(System.in).nextLine();
			server.shutdown(1, TimeUnit.MINUTES);

		} catch (Exception e) {
			if (server != null) {
				server.shutdownNow();
			}
			throw new ServerError("Server unexpected stop working", e);
		}
	}

	private void registerServletsMappingHandlers(HttpServer server) {
		for (Deployment deployment : applications) {
			for (ServletMappingHandler<? extends GodServlet> handler : deployment.getHandlers()) {
				server.getServerConfiguration().addHttpHandler(handler, handler.getMappingPath());
			}
		}

	}
}
