package cz.svetonaut.aserver.container.deploy;

public class DeployFailedException extends RuntimeException {
	public DeployFailedException() {
	}

	public DeployFailedException(String message) {
		super(message);
	}

	public DeployFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public DeployFailedException(Throwable cause) {
		super(cause);
	}
}
