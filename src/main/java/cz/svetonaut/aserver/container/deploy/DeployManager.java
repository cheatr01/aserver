package cz.svetonaut.aserver.container.deploy;

import cz.svetonaut.aserver.configuration.Configuration;
import cz.svetonaut.aserver.deployment.Deployment;
import cz.svetonaut.aserver.deployment.DeploymentLoadingException;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class DeployManager {

	private final Configuration configuration;
	private final Set<Deployment> applications = new HashSet<>();

	public DeployManager(Configuration configuration) {
		this.configuration = configuration;
	}

	private Set<Path> searchForJar(String sourcePathString) {
		try {
			final Path sourcePath = Path.of(sourcePathString);
			System.out.println("Current sourcePath is: " + new File("./").getAbsolutePath());
			if (!sourcePath.toFile().isDirectory()) {
				return Set.of();
			}

			return Files.walk(sourcePath)
					.filter(Files::isRegularFile)
					.filter(it -> it.toString().endsWith(".jar"))
					.collect(Collectors.toSet());
		} catch (Exception e) {
			throw new JarProcessingExceptions(e);
		}
	}

	public Set<Deployment> deployAll() {
		try {
			Set<Path> jars = searchForJar(configuration.getDeplSourcePath());

			for (Path jar : jars) {
				try {

					final Deployment deployment = new Deployment(jar, configuration);
					deployment.deploy();
					applications.add(deployment);

				} catch (DeploymentLoadingException e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			throw new DeployFailedException(e);
		}
		return applications;
	}
}
