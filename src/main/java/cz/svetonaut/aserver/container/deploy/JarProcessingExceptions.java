package cz.svetonaut.aserver.container.deploy;

public class JarProcessingExceptions extends RuntimeException {
	public JarProcessingExceptions() {
	}

	public JarProcessingExceptions(String message) {
		super(message);
	}

	public JarProcessingExceptions(String message, Throwable cause) {
		super(message, cause);
	}

	public JarProcessingExceptions(Throwable cause) {
		super(cause);
	}
}
