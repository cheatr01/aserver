package cz.svetonaut.aserver.container;

import cz.svetonaut.aserver.framework.GodServlet;
import org.glassfish.grizzly.http.Method;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

public class ServletMappingHandler<T extends GodServlet> extends HttpHandler {

	private final String mappingPath;
	private final Class<T> bean;

	public ServletMappingHandler(String mappingPath, Class<T> bean) {
		this.mappingPath = mappingPath;
		this.bean = bean;
	}

	public String getMappingPath() {
		return mappingPath;
	}

	@Override
	public void service(Request request, Response response) throws Exception {
		final GodServlet instance = bean.getConstructor().newInstance();
		final Method method = request.getMethod();

		if (method == Method.GET) {
			instance.doGet(request, response);
		} else if (method == Method.POST) {
			instance.doPost(request, response);
		}
	}
}
