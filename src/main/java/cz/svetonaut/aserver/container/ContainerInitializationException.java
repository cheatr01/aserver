package cz.svetonaut.aserver.container;

public class ContainerInitializationException extends RuntimeException {
	public ContainerInitializationException() {
	}

	public ContainerInitializationException(String message) {
		super(message);
	}

	public ContainerInitializationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ContainerInitializationException(Throwable cause) {
		super(cause);
	}
}
