package cz.svetonaut.aserver.framework;

import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;

public interface GodServlet {

	void doGet(Request request, Response response);

	void doPost(Request request, Response response);
}
