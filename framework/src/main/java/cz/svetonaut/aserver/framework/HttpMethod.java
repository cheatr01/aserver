package cz.svetonaut.aserver.framework;

public enum HttpMethod {
	GET,
	POST,
	PUT,
	DELETE,
	;
}
